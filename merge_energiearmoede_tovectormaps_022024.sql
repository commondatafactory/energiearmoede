/*
 * 	Below code is documenting the battle to connect CBS dataset to another CBS dataset containing geographic neighborhood names.
 *
 *    - regiocode is matching correctluy with 2023 cbs admin areas.
 *    - fix . is null
 *    - fix nummeric values
 *
 **/


create schema if not exists cbsarmoede;

drop table if exists cbsarmoede.monitor_energiearmoede_2021;

select *
into cbsarmoede.monitor_energiearmoede_2021
from import.monitor_energiearmoede_2021 me ;

ALTER TABLE cbsarmoede.monitor_energiearmoede_2021
ADD COLUMN id SERIAL PRIMARY KEY;

alter table cbsarmoede.monitor_energiearmoede_2021
add column buurtcode text;

alter table cbsarmoede.monitor_energiearmoede_2021
add column wijkcode text;

alter table cbsarmoede.monitor_energiearmoede_2021
add column gemeentecode text;


create index on cbsarmoede.monitor_energiearmoede_2021 (buurtcode);
create index on cbsarmoede.monitor_energiearmoede_2021 (wijkcode);

select * from cbsarmoede.monitor_energiearmoede_2021 me;
select * from cbs2023.cdf_gem cg;


/* testing regiocode matching */

select count(*)
from cbsarmoede.monitor_energiearmoede_2021 me
left outer join cbs2023.buurten b on concat('BU', me.regiocode) = b.buurtcode
where b.buurtcode is not null


/* UPDATE adminstration columns */

/* match each regiocode with the cbs2023.buurten geo dataset */

update cbsarmoede.monitor_energiearmoede_2021 me
set buurtcode = b.buurtcode
from (
    select me.id, b.buurtcode from cbsarmoede.monitor_energiearmoede_2021 me
    left outer join cbs2023.buurten b on concat('BU', me.regiocode) = b.buurtcode
    where b.buurtcode is not null
) b
where b.id = me.id;


update cbsarmoede.monitor_energiearmoede_2021 me
set wijkcode = w.wijkcode
from (
    select me.id, w.wijkcode from cbsarmoede.monitor_energiearmoede_2021 me
    left outer join cbs2023.wijken w on concat('WK', me.regiocode) = w.wijkcode
    where w.wijkcode is not null
    ) w
where w.id = me.id;

/* match each name with best matching name from the cbs2022.buurten geo dataset */

update cbsarmoede.monitor_energiearmoede_2021 me
set gemeentecode = g.gemeentecode
from (
    select me.id, g.gemeentecode from cbsarmoede.monitor_energiearmoede_2021 me
	left outer join cbs2022.gemeenten g on concat('GM', me.regiocode) = g.gemeentecode
    where g.gemeentecode is not null
    ) g
where g.id = me.id;



select buurtcode, * from cbsarmoede.monitor_energiearmoede_2021 me where buurt != 'Totaal' and buurtcode is not null;

select * from cbsarmoede.monitor_energiearmoede_2021 me where buurt = 'Totaal';
select * from cbsarmoede.monitor_energiearmoede_2021 me where gemeentecode is not null;
select * from cbsarmoede.monitor_energiearmoede_2021 me where buurtcode is not null;
select * from cbsarmoede.monitor_energiearmoede_2021 me where wijkcode is not null;

drop table if exists cbsarmoede.test;
select * into cbsarmoede.test from cbsarmoede.monitor_energiearmoede_2021 me;
select * from cbsarmoede.monitor_energiearmoede_2021 me ;
select * from cbsarmoede.test;

/* clean spaces and numbers */
update cbsarmoede.monitor_energiearmoede_2021
--update cbsarmoede.test
  set aantal_huishoudens1 = replace(replace(aantal_huishoudens1, ',', '.'), ' ', ''),
      energiearmoede_lihelek2  = replace(replace(energiearmoede_lihelek2, ',', '.'), ' ', ''),
      energiearmoede_heq  = replace(replace(energiearmoede_heq, ',', '.'), ' ', ''),
      energiearmoede_lihe  = replace(replace(energiearmoede_lihe, ',', '.'), ' ', ''),
      energiearmoede_lilek = replace(replace(energiearmoede_lilek, ',', '.'), ' ', ''),
      energiearmoede_lekwi  = replace(replace(energiearmoede_lekwi, ',', '.'), ' ', ''),
      residueel_inkomen  = replace(replace(residueel_inkomen, ',', '.'), ' ', ''),
      energiequote  = replace(replace(energiequote , ',', '.'), ' ', '');

/* turn dots '.' into null values */
update cbsarmoede.monitor_energiearmoede_2021
--update cbsarmoede.test
  set aantal_huishoudens1 = NULLIF(aantal_huishoudens1, '.'),
      energiearmoede_lihelek2  = NULLIF(energiearmoede_lihelek2, '.'),
      energiearmoede_heq  = NULLIF(energiearmoede_heq, '.'),
      energiearmoede_lihe  = NULLIF(energiearmoede_lihe, '.'),
      energiearmoede_lilek = NULLIF(energiearmoede_lilek, '.'),
      energiearmoede_lekwi  = NULLIF(energiearmoede_lekwi, '.'),
      residueel_inkomen  = NULLIF(residueel_inkomen, '.'),
      energiequote  = NULLIF(energiequote , '.');

/


select * from cbsarmoede.monitor_energiearmoede_2021 me;

select * from cbsarmoede.test;
/* turn string into nice numeric values */
-- ALTER table cbsarmoede.monitor_energiearmoede_2021
alter table cbsarmoede.monitor_energiearmoede_2021
ALTER COLUMN aantal_huishoudens1 type numeric(5, 1) using aantal_huishoudens1::numeric(5,1),
ALTER COLUMN energiearmoede_lihelek2 type numeric(4, 1) using energiearmoede_lihelek2::numeric(4,1),
ALTER COLUMN energiearmoede_heq type numeric(4, 1) using energiearmoede_heq::numeric(4,1),
ALTER COLUMN energiearmoede_lihe type numeric(4, 1) using energiearmoede_lihe::numeric(4,1),
ALTER COLUMN energiearmoede_lilek type numeric(4, 1) using energiearmoede_lilek::numeric(4,1),
ALTER COLUMN energiearmoede_lekwi type numeric(4, 1) using energiearmoede_lekwi::numeric(4,1),
ALTER COLUMN residueel_inkomen type numeric(4, 1) using residueel_inkomen::numeric(4,1),
ALTER COLUMN energiequote type numeric(4, 1) using energiequote::numeric(4,1)


select * from cbsarmoede.monitor_energiearmoede_2021 me

select * from cbsarmoede.test;

select distinct(soort_eigendom_woning) from cbsarmoede.monitor_energiearmoede_2021;

drop table if exists cbsarmoede.energiearmoede_vectordata;


select
    gemeentecode,
    wijkcode,
    buurtcode,
    max(case when me.soort_eigendom_woning = 'Koop' then me.aantal_huishoudens1 else null end) as koop_huishoudens,
    max(case when me.soort_eigendom_woning = 'Koop' then me.energiearmoede_lihelek2 else null end) as koop_lihelek2,
    max(case when me.soort_eigendom_woning = 'Koop' then me.energiearmoede_heq else null end) as koop_heq,
    max(case when me.soort_eigendom_woning = 'Koop' then me.energiearmoede_lihe else null end) as koop_lihe,
    max(case when me.soort_eigendom_woning = 'Koop' then me.energiearmoede_lilek else null end) as koop_lilek,
    max(case when me.soort_eigendom_woning = 'Koop' then me.energiearmoede_lekwi else null end) as koop_lekwi,
    max(case when me.soort_eigendom_woning = 'Koop' then me.residueel_inkomen else null end) as koop_rest_ink,
    max(case when me.soort_eigendom_woning = 'Koop' then me.energiequote else null end) as koop_eq,
    max(case when me.soort_eigendom_woning = 'Totaal' then me.aantal_huishoudens1 else null end) as tot_huishoudens,
    max(case when me.soort_eigendom_woning = 'Totaal' then me.energiearmoede_lihelek2 else null end) as tot_lihelek2,
    max(case when me.soort_eigendom_woning = 'Totaal' then me.energiearmoede_heq else null end) as tot_heq,
    max(case when me.soort_eigendom_woning = 'Totaal' then me.energiearmoede_lihe else null end) as tot_lihe,
    max(case when me.soort_eigendom_woning = 'Totaal' then me.energiearmoede_lilek else null end) as tot_lilek,
    max(case when me.soort_eigendom_woning = 'Totaal' then me.energiearmoede_lekwi else null end) as tot_lekwi,
    max(case when me.soort_eigendom_woning = 'Totaal' then me.residueel_inkomen else null end) as tot_rest_ink,
    max(case when me.soort_eigendom_woning = 'Totaal' then me.energiequote else null end) as tot_eq,
    max(case when me.soort_eigendom_woning = 'Huur, overig' then me.aantal_huishoudens1 else null end) as huur_o_huishoudens,
    max(case when me.soort_eigendom_woning = 'Huur, overig' then me.energiearmoede_lihelek2 else null end) as huur_o_lihelek2,
    max(case when me.soort_eigendom_woning = 'Huur, overig' then me.energiearmoede_heq else null end) as huur_o_heq,
    max(case when me.soort_eigendom_woning = 'Huur, overig' then me.energiearmoede_lihe else null end) as huur_o_lihe,
    max(case when me.soort_eigendom_woning = 'Huur, overig' then me.energiearmoede_lilek else null end) as huur_o_lilek,
    max(case when me.soort_eigendom_woning = 'Huur, overig' then me.energiearmoede_lekwi else null end) as huur_o_lekwi,
    max(case when me.soort_eigendom_woning = 'Huur, overig' then me.residueel_inkomen else null end) as huur_o_rest_ink,
    max(case when me.soort_eigendom_woning = 'Huur, overig' then me.energiequote else null end) as huur_o_eq,
    max(case when me.soort_eigendom_woning = 'Huur, corporatie' then me.aantal_huishoudens1 else null end) as huur_c_huishoudens,
    max(case when me.soort_eigendom_woning = 'Huur, corporatie' then me.energiearmoede_lihelek2 else null end) as huur_c_lihelek2,
    max(case when me.soort_eigendom_woning = 'Huur, corporatie' then me.energiearmoede_heq else null end) as huur_c_heq,
    max(case when me.soort_eigendom_woning = 'Huur, corporatie' then me.energiearmoede_lihe else null end) as huur_c_lihe,
    max(case when me.soort_eigendom_woning = 'Huur, corporatie' then me.energiearmoede_lilek else null end) as huur_c_lilek,
    max(case when me.soort_eigendom_woning = 'Huur, corporatie' then me.energiearmoede_lekwi else null end) as huur_c_lekwi,
    max(case when me.soort_eigendom_woning = 'Huur, corporatie' then me.residueel_inkomen else null end) as huur_c_rest_ink,
    max(case when me.soort_eigendom_woning = 'Huur, corporatie' then me.energiequote else null end) as huur_c_eq,
    max(case when me.soort_eigendom_woning = 'Onbekend' then me.aantal_huishoudens1 else null end) as onb_huishoudens,
    max(case when me.soort_eigendom_woning = 'Onbekend' then me.energiearmoede_lihelek2 else null end) as onb_lihelek2,
    max(case when me.soort_eigendom_woning = 'Onbekend' then me.energiearmoede_heq else null end) as onb_heq,
    max(case when me.soort_eigendom_woning = 'Onbekend' then me.energiearmoede_lihe else null end) as onb_lihe,
    max(case when me.soort_eigendom_woning = 'Onbekend' then me.energiearmoede_lilek else null end) as onb_lilek,
    max(case when me.soort_eigendom_woning = 'Onbekend' then me.energiearmoede_lekwi else null end) as onb_lekwi,
    max(case when me.soort_eigendom_woning = 'Onbekend' then me.residueel_inkomen else null end) as onb_rest_ink,
    max(case when me.soort_eigendom_woning = 'Onbekend' then me.energiequote else null end) as onb_eq
into cbsarmoede.energiearmoede_vectordata_2021
from cbsarmoede.monitor_energiearmoede_2021 me group by gemeentecode, wijkcode, buurtcode;


create index on cbsarmoede.energiearmoede_vectordata_2021 (gemeentecode);
create index on cbsarmoede.energiearmoede_vectordata_2021 (wijkcode);
create index on cbsarmoede.energiearmoede_vectordata_2021 (buurtcode);

/* test the output */
select * from cbsarmoede.energiearmoede_vectordata_2021 ev;
