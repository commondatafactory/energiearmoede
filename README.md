## Energie Armoede

Import energie armoede dataset into database make energiearmoede data usable for our mapping tools.

source:

[CBS 2021](https://www.cbs.nl/nl-nl/maatwerk/2023/47/monitor-energiearmoede-2021)

[CBS](https://www.cbs.nl/nl-nl/corporate/2023/04/cbs-en-tno-brengen-energiearmoede-huishoudens-in-kaart)


## Description

CBS provides an xlsx file which we transform here to vector tiles ready to be used in
dego.vng.nl and wijkpaspoort.vng.nl.

## Installation

Run and read the shell scripts.


## Contributing

pull request.

## Authors and acknowledgment

Many thanks to the CBS.nl teams providing data to work with.

