#!/bin/bash
set -u
set -e
set -x


xlsx2csv --no-line-breaks -n "Tabel 3" Tabellenset_MonitorEnergiearmoede_v4.xlsx monitor_energiearmoede.csv
