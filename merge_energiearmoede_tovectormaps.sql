/*
 * 	below code is documenting the battle to connect CBS dataset to another CBS dataset containing geographic neighborhood names.
 *
 *  name differences found beteween cbs names in data and cbs names exposed in geographic dataset...
 *
	'verspr.h.', 'verspreide huizen'),
	'bedrijventerr.', 'bedrijventerrein'),
	'e.o.', 'en omgeving'),
	'omg.', 'omgeving '),
	'buitengeb.', 'buitengebied'),
	'v.', 'van'),
	'n.', 'noorden'),
	'z.', 'zuiden'),
	'w.', 'westen'),
	' ', ''),
	'''', ''),

*
**/


/* SOLUTION */
create extension fuzzystrmatch;

create schema if not exists cbsarmoede;

drop table if exists cbsarmoede.monitor_energiearmoede;

select *
into cbsarmoede.monitor_energiearmoede
from import.monitor_energiearmoede me ;

ALTER TABLE cbsarmoede.monitor_energiearmoede
ADD COLUMN id SERIAL PRIMARY KEY;

alter table cbsarmoede.monitor_energiearmoede
add column buurtcode text;

alter table cbsarmoede.monitor_energiearmoede
add column wijkcode text;

alter table cbsarmoede.monitor_energiearmoede
add column gemeentecode text;


create index on cbsarmoede.monitor_energiearmoede (buurtcode);
create index on cbsarmoede.monitor_energiearmoede (wijkcode);


/* testing name matching, visualizes the worst cases wich need more then 5 edits before matching */

select me.buurt, b.buurtnaam, b.match_score
from cbsarmoede.monitor_energiearmoede me,
lateral ( select
                b.buurtnaam,
                levenshtein(b.buurtnaam, me.buurt) as match_score
          from cbs2022.buurten b
          where me.gemeente = b.gemeentenaam and me.buurt != 'Totaal' --and me.type_eigenaar = 'Woningeigenaar'
          order by levenshtein(b.buurtnaam, me.buurt)  asc
          limit 1
) b
where b.match_score > 5;

/* testing matching of wijknamen */
select me.wijk, b.wijknaam, b.match_score
from cbsarmoede.monitor_energiearmoede me,
lateral ( select
                w.wijknaam,
                levenshtein(w.wijknaam, me.wijk) as match_score
          from cbs2022.wijken w
          where me.gemeente = w.gemeentenaam and me.buurt = 'Totaal'and me.wijk != 'Totaal'--and me.type_eigenaar = 'Woningeigenaar'
          order by levenshtein(w.wijknaam, me.wijk)  asc
          limit 1
) b
where b.match_score > 5;


/* match each name with best matching name from the cbs2022.buurten geo dataset */


update cbsarmoede.monitor_energiearmoede me
set buurtcode = subquery.buurtcode
from (
    select me.id, b.buurtcode from cbsarmoede.monitor_energiearmoede me,
    lateral (
	select buurtcode from cbs2022.buurten b where me.gemeente = b.gemeentenaam and me.buurt != 'Totaal'
	order by levenshtein(b.buurtnaam, me.buurt)  asc
    limit 1
    ) b
) subquery
where subquery.id = me.id;



update cbsarmoede.monitor_energiearmoede me
set wijkcode = subquery.wijkcode
from (
    select me.id, w.wijkcode from cbsarmoede.monitor_energiearmoede me,
    lateral (
	select wijkcode from cbs2022.wijken b where me.gemeente = b.gemeentenaam and me.wijk != 'Totaal' and me.buurt = 'Totaal'
	order by levenshtein(b.wijknaam, me.wijk)  asc
    limit 1
    ) w
) subquery
where subquery.id = me.id;

/* match each name with best matching name from the cbs2022.buurten geo dataset */


update cbsarmoede.monitor_energiearmoede me
set gemeentecode = subquery.gemeentecode
from (
    select me.id, g.gemeentecode from cbsarmoede.monitor_energiearmoede me,
    lateral (
	select gemeentecode from cbs2022.gemeenten g where me.gemeente = g.gemeentenaam and me.wijk = 'Totaal' and me.buurt = 'Totaal'
    limit 1
    ) g
) subquery
where subquery.id = me.id;



select buurtcode, * from import.monitor_energiearmoede me where buurt != 'Totaal' and buurtcode is not null;

select * from cbsarmoede.monitor_energiearmoede me where buurt = 'Totaal';

select * from cbsarmoede.monitor_energiearmoede me where gemeentecode is not null

drop table if exists cbsarmoede.test;
select * into cbsarmoede.test from cbsarmoede.monitor_energiearmoede me;

/* turn dots '.' into null values */
update cbsarmoede.monitor_energiearmoede
  set aantal_huishoudens1_x_1000 = NULLIF(aantal_huishoudens1_x_1000, '.'),
      energiearmoede_volgens_heq_2  = NULLIF(energiearmoede_volgens_heq_2, '.'),
      energiearmoede_volgens_lihe_2  = NULLIF(energiearmoede_volgens_lihe_2, '.'),
      energiearmoede_volgens_lilek_2  = NULLIF(energiearmoede_volgens_lilek_2, '.'),
      energiearmoede_volgens_lekwi_2 = NULLIF(energiearmoede_volgens_lekwi_2, '.'),
      energiearmoede_volgens_lihe_en_of_lilek_2  = NULLIF(energiearmoede_volgens_lihe_en_of_lilek_2, '.'),
      gemiddeld_gestandaardiseerd_residueel_inkomen__x_1_000_euro  = NULLIF(gemiddeld_gestandaardiseerd_residueel_inkomen__x_1_000_euro, '.'),
      gemiddelde_energiequote_  = NULLIF(gemiddelde_energiequote_ , '.');

/* turn string into nice numeric values */
ALTER table cbsarmoede.monitor_energiearmoede
ALTER COLUMN aantal_huishoudens1_x_1000 type numeric(10, 1) using aantal_huishoudens1_x_1000::numeric(10,1),
ALTER COLUMN energiearmoede_volgens_heq_2 type numeric(4, 1) using energiearmoede_volgens_heq_2::numeric(4,1),
ALTER COLUMN energiearmoede_volgens_lihe_2 type numeric(4, 1) using energiearmoede_volgens_lihe_2::numeric(4,1),
ALTER COLUMN energiearmoede_volgens_lilek_2 type numeric(4, 1) using energiearmoede_volgens_lilek_2::numeric(4,1),
ALTER COLUMN energiearmoede_volgens_lekwi_2 type numeric(4, 1) using energiearmoede_volgens_lekwi_2::numeric(4,1),
ALTER COLUMN energiearmoede_volgens_lihe_en_of_lilek_2 type numeric(4, 1) using energiearmoede_volgens_lihe_en_of_lilek_2::numeric(4,1),
ALTER COLUMN gemiddeld_gestandaardiseerd_residueel_inkomen__x_1_000_euro type numeric(4, 1) using gemiddeld_gestandaardiseerd_residueel_inkomen__x_1_000_euro::numeric(4,1),
ALTER COLUMN gemiddelde_energiequote_ type numeric(4, 1) using gemiddelde_energiequote_::numeric(4,1)



select * from cbsarmoede.monitor_energiearmoede me;


select distinct(type_eigenaar) from cbs2022.wijken w left outer join cbsarmoede.monitor_energiearmoede me on me.wijkcode = w.wijkcode where me.periode = '2020';

drop table if exists cbsarmoede.energiearmoede_vectordata;

select
    periode,
    gemeentecode,
    wijkcode,
	buurtcode,
	max(case when me.type_eigenaar = 'Woningeigenaar' then me.aantal_huishoudens1_x_1000 else null end) as won_huishoudens,
	max(case when me.type_eigenaar = 'Woningeigenaar' then me.energiearmoede_volgens_heq_2 else null end) as won_heq,
	max(case when me.type_eigenaar = 'Woningeigenaar' then me.energiearmoede_volgens_lihe_2 else null end) as won_lihe,
	max(case when me.type_eigenaar = 'Woningeigenaar' then me.energiearmoede_volgens_lilek_2 else null end) as won_lilek,
	max(case when me.type_eigenaar = 'Woningeigenaar' then me.energiearmoede_volgens_lekwi_2 else null end) as won_lekwi,
	max(case when me.type_eigenaar = 'Woningeigenaar' then me.energiearmoede_volgens_lihe_en_of_lilek_2 else null end) as won_lihe_lilek,
    max(case when me.type_eigenaar = 'Woningeigenaar' then me.gemiddeld_gestandaardiseerd_residueel_inkomen__x_1_000_euro else null end) as won_rest_ink,
    max(case when me.type_eigenaar = 'Woningeigenaar' then me.gemiddelde_energiequote_ else null end) as won_eq,
	max(case when me.type_eigenaar = 'Totaal' then me.aantal_huishoudens1_x_1000 else null end) as tot_huishoudens,
	max(case when me.type_eigenaar = 'Totaal' then me.energiearmoede_volgens_heq_2 else null end) as tot_heq,
	max(case when me.type_eigenaar = 'Totaal' then me.energiearmoede_volgens_lihe_2 else null end) as tot_lihe,
	max(case when me.type_eigenaar = 'Totaal' then me.energiearmoede_volgens_lilek_2 else null end) as tot_lilek,
	max(case when me.type_eigenaar = 'Totaal' then me.energiearmoede_volgens_lekwi_2 else null end) as tot_lekwi,
	max(case when me.type_eigenaar = 'Totaal' then me.energiearmoede_volgens_lihe_en_of_lilek_2 else null end) as tot_lihe_lilek,
    max(case when me.type_eigenaar = 'Totaal' then me.gemiddeld_gestandaardiseerd_residueel_inkomen__x_1_000_euro else null end) as tot_rest_ink,
    max(case when me.type_eigenaar = 'Totaal' then me.gemiddelde_energiequote_ else null end) as tot_eq,
	max(case when me.type_eigenaar = 'Huur (overige verhuurders)' then me.aantal_huishoudens1_x_1000 else null end) as huur_o_huishoudens,
	max(case when me.type_eigenaar = 'Huur (overige verhuurders)' then me.energiearmoede_volgens_heq_2 else null end) as huur_o_heq,
	max(case when me.type_eigenaar = 'Huur (overige verhuurders)' then me.energiearmoede_volgens_lihe_2 else null end) as huur_o_lihe,
	max(case when me.type_eigenaar = 'Huur (overige verhuurders)' then me.energiearmoede_volgens_lilek_2 else null end) as huur_o_lilek,
    max(case when me.type_eigenaar = 'Huur (overige verhuurders)' then me.energiearmoede_volgens_lekwi_2 else null end) as huur_o_lekwi,
	max(case when me.type_eigenaar = 'Huur (overige verhuurders)' then me.energiearmoede_volgens_lihe_en_of_lilek_2 else null end) as huur_o_lihe_lilek,
    max(case when me.type_eigenaar = 'Huur (overige verhuurders)' then me.gemiddeld_gestandaardiseerd_residueel_inkomen__x_1_000_euro else null end) as huur_o_rest_ink,
    max(case when me.type_eigenaar = 'Huur (overige verhuurders)' then me.gemiddelde_energiequote_ else null end) as huur_o_eq,
	max(case when me.type_eigenaar = 'Huur (woningcorporatie)' then me.aantal_huishoudens1_x_1000 else null end) as huur_c_huishoudens,
	max(case when me.type_eigenaar = 'Huur (woningcorporatie)' then me.energiearmoede_volgens_heq_2 else null end) as huur_c_heq,
	max(case when me.type_eigenaar = 'Huur (woningcorporatie)' then me.energiearmoede_volgens_lihe_2 else null end) as huur_c_lihe,
	max(case when me.type_eigenaar = 'Huur (woningcorporatie)' then me.energiearmoede_volgens_lilek_2 else null end) as huur_c_lilek,
	max(case when me.type_eigenaar = 'Huur (woningcorporatie)' then me.energiearmoede_volgens_lekwi_2 else null end) as huur_c_lekwi,
	max(case when me.type_eigenaar = 'Huur (woningcorporatie)' then me.energiearmoede_volgens_lihe_en_of_lilek_2 else null end) as huur_c_lihe_lilek,
    max(case when me.type_eigenaar = 'Huur (woningcorporatie)' then me.gemiddeld_gestandaardiseerd_residueel_inkomen__x_1_000_euro else null end) as huur_c_rest_ink,
    max(case when me.type_eigenaar = 'Huur (woningcorporatie)' then me.gemiddelde_energiequote_ else null end) as huur_c_eq,
	max(case when me.type_eigenaar = 'Onbekend' then me.aantal_huishoudens1_x_1000 else null end) as onb_huishoudens,
	max(case when me.type_eigenaar = 'Onbekend' then me.energiearmoede_volgens_heq_2 else null end) as onb_heq,
	max(case when me.type_eigenaar = 'Onbekend' then me.energiearmoede_volgens_lihe_2 else null end) as onb_lihe,
	max(case when me.type_eigenaar = 'Onbekend' then me.energiearmoede_volgens_lilek_2 else null end) as onb_lilek,
	max(case when me.type_eigenaar = 'Onbekend' then me.energiearmoede_volgens_lekwi_2 else null end) as onb_lekwi,
	max(case when me.type_eigenaar = 'Onbekend' then me.energiearmoede_volgens_lihe_en_of_lilek_2 else null end) as onb_lihe_lilek,
    max(case when me.type_eigenaar = 'Onbekend' then me.gemiddeld_gestandaardiseerd_residueel_inkomen__x_1_000_euro else null end) as onb_rest_ink,
    max(case when me.type_eigenaar = 'Onbekend' then me.gemiddelde_energiequote_ else null end) as onb_eq
into cbsarmoede.energiearmoede_vectordata
from cbsarmoede.monitor_energiearmoede me group by periode, gemeentecode, wijkcode, buurtcode;

create index on cbsarmoede.energiearmoede_vectordata (gemeentecode);
create index on cbsarmoede.energiearmoede_vectordata (wijkcode);
create index on cbsarmoede.energiearmoede_vectordata (buurtcode);

/* test the output */
select * from cbsarmoede.energiearmoede_vectordata ev;

/*
select won_huishoudens, count(*) from cbsarmoede.energiearmoede_vectordata2 group by won_huishoudens order by won_huishoudens  desc;
select won_heq, count(*) from cbsarmoede.energiearmoede_vectordata2 group by won_heq order by won_heq  desc;
select won_heq, count(*) from cbsarmoede.energiearmoede_vectordata group by won_heq order by won_heq  desc;
*/


/* create a nice numeric views for this */

select version();
