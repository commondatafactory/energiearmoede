#!/bin/bash
set -u
set -e
set -x

pgfutter --port 5433 --dbname cdf --username cdf  --pass insecure csv -d ';' monitor_energiearmoede_2021_v2.csv
