#!/bin/bash
set -u
set -e
set -x

echo "starting import"

# Get CBS source data if not already downloaded.
# curl -O 'https://www.cbs.nl/-/media/_excel/2024/05/240130_monitorenergiearmoede2021.xlsx'

curl -O 'https://download.cbs.nl/maatwerk/MonitorEnergiearmoede2019-2022_def.xlsx'

echo "you need to remove header and trailing lines manually!!"
echo "renmme columns for pg import"

# use libreoffice export to create export of sheet..
# xlsx2csv --no-line-breaks -n "Tabel 3" -d ";" 240130_MonitorEnergiearmoede2021.xlsx  monitor_energiearmoede_2021_needsedits.csv

